# Dynamic vision sensor - setup

## Instalation

#### Docker

If you haven't installed docker, you can find some help here: https://docs.docker.com/cs-engine/1.12/

##### Clone and enter the gitlab repository

    git clone https://gitlab.esrl.dk/SDU-embedded/neuromorphic/DynamicVisionSensor-setup
    cd ./DynamicVisionSensor-setup/

##### Build the docker image and run the container

When executed, the script 'run.sh' tries to attach to a current running container, if that fails, it tries to startup a closed container, if that fails, it tries to run a new container and if that fails, it will finally build a new image.

This means no matter of a container is running, stopped or does not exist, you can execute ./run.sh in order to get access to a running container.

    ./run.sh

Two dockerfiles is developped. One based on Ubuntu 18.04 (Bionic Beaver) and one on Debian 9 (Stretch).

(The one based on Ubuntu can't run jaer due to a uncaught java exception)

The version is defined with either a -d as option for Debian:

    ./run.sh -d

Or a -u for Ubuntu:

    ./run.sh -u

Note: In order to give the container access to the dvs's device file, the device should be pluged in before starting the container.

### Driver installation

Create the device rule:

    Create the file "/etc/udev/rules.d/65-inilabs.rules" on the host mashine, with the following content:

    # All DVS/DAVIS systems
    SUBSYSTEM=="usb", ATTR{idVendor}=="152a", ATTR{idProduct}=="84[0-1]?", MODE="0666"

To reload the udev system without rebooting type, as root:

    udevadm control --reload-rules

or, for newer udev versions:

    udevadm control --reload

Now unplug and replug the camera into your computer. You’re done!

###### Start jaer

In order to start jaer, go into the folder and run the shell script:

    cd jaer
    ./jAERViewer_linux.sh

This does not work in the ubuntu version yet due to some un caught java exceptions.

## jaer

jaer is a great demo program for showing the dvs's data on a computer. The program can capture both the standard images as well as the events. Further more it reads out the imu values from the built in imu.

A lot of settings can be tuned in jaer including the biases and visualization modes.

First time jaer is started, two things need to be done:

1. Selecting the AEChip:

    Click: alt + a
    Select: "eu.seebetter.ini.chips.davis.DAVIS240C"

2. Load settings:

    Click: ctrl + b
    Click: alt + f
    Select: "load settings..."
    Open the file: ~/jaer/biasgenSettings/Davis240bc/Davis240c_Test_NoAuto.xml

Now the window should show a live feed from the camera.

The default feed shows the camera frames with the events, and the imu as a overlay on it.

The frames can be toggled by:

    Clicking: shift + f

The events can be toogled by:

    Clicking: shift + e

The imu can be toogled by:

    Clicking: shift + i

For more graphic options see the "View" tab (alt + v) or the "DAVIS" tab.

### Flashy

Flashy is a small program which is included in the docker container and is used to flash new firmware onto the DVS. This is not nessesary unles jaer have told you so.

In order to flash new firmware to the DVS, start the program:

    cd flashy
    ./Flashy-1.2.0-jar-with-dependencies.jar
    
And click "Update logic".

After this process, close the program, unplug the DVS and re'plug the DVS.

The firmware is now newest version.    

## libcaer

libcaer is a c-library for extracting aer events from aer devices. In this case it is used to extract pixel events from the DVS240C. Further more it can be used for extraction of the imu values from the built in imu.

### Examples

The example folder in the home directory includes example applications which uses libcaer.

In order to compile and execute the examples, navigate into the desired example and run:

    make && make run

## DVS AER (gpios)

Connecting the dvs to a computer using a usb-cable and running a application on a computer is a easy way of extracting data from the dvs, but in a real time application it is not the optimal solution. Therefore the dvs features the aer interface through gpios. This interface require a low-level aer receiver.

### FPGA dvs aer receiver

A module in vhdl is developped in order to extract events from the dvs as fast as posible.



### PCB
The dvs aer PCB can be plugged directly into the PMOD connectors of digilents FPGAs. The PCB uses tho pmods and features both AER for pixel events and I^2 C for IMU data extraction.

The PCB should be pluged into the PMODs with the connectors pointing upwards (see image below).

# Insert image

The constraint propeties depends on the FPGA and the used ports on the different FPGAs.

##### PMOD B and C (ZYBO)

    #Pmod Header JB
    set_property -dict { PACKAGE_PIN T20   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[8] }]; #IO_L5P_T0_34 Sch=jd_p[1]
    set_property -dict { PACKAGE_PIN U20   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[9] }]; #IO_L5N_T0_34 Sch=jd_n[1]
    set_property -dict { PACKAGE_PIN V20   IOSTANDARD LVCMOS33     } [get_ports { scl }]; #IO_L6P_T0_34 Sch=jd_p[2]
    set_property -dict { PACKAGE_PIN W20   IOSTANDARD LVCMOS33     } [get_ports { sda }]; #IO_L6N_T0_VREF_34 Sch=jd_n[2]
    set_property -dict { PACKAGE_PIN Y18   IOSTANDARD LVCMOS33     } [get_ports { dvs_n_ack }]; #IO_L11P_T1_SRCC_34 Sch=jd_p[3]
    set_property -dict { PACKAGE_PIN Y19   IOSTANDARD LVCMOS33     } [get_ports { dvs_n_req }]; #IO_L11N_T1_SRCC_34 Sch=jd_n[3]
    set_property -dict { PACKAGE_PIN W18   IOSTANDARD LVCMOS33     } [get_ports { imuint }]; #IO_L21P_T3_DQS_34 Sch=jd_p[4]
    set_property -dict { PACKAGE_PIN W19   IOSTANDARD LVCMOS33     } [get_ports { fsync }]; #IO_L21N_T3_DQS_34 Sch=jd_n[4]
    
    #Pmod Header JC
    set_property -dict { PACKAGE_PIN V15   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[0] }]; #IO_L4P_T0_34 Sch=je[1]
    set_property -dict { PACKAGE_PIN W15   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[1] }]; #IO_L18N_T2_34 Sch=je[2]
    set_property -dict { PACKAGE_PIN T11   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[2] }]; #IO_25_35 Sch=je[3]
    set_property -dict { PACKAGE_PIN T10   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[3] }]; #IO_L19P_T3_35 Sch=je[4]
    set_property -dict { PACKAGE_PIN W14   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[4] }]; #IO_L3N_T0_DQS_34 Sch=je[7]
    set_property -dict { PACKAGE_PIN Y14   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[5] }]; #IO_L9N_T1_DQS_34 Sch=je[8]
    set_property -dict { PACKAGE_PIN T12   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[6] }]; #IO_L20P_T3_34 Sch=je[9]
    set_property -dict { PACKAGE_PIN U12   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[7] }]; #IO_L7N_T1_34 Sch=je[10]

##### PMOD B and C (ZYBO Z7-20)

    #Pmod Header JB
    set_property -dict { PACKAGE_PIN V8    IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[8] }]; #IO_L5P_T0_34 Sch=jd_p[1]
    set_property -dict { PACKAGE_PIN W8    IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[9] }]; #IO_L5N_T0_34 Sch=jd_n[1]
    set_property -dict { PACKAGE_PIN U7    IOSTANDARD LVCMOS33     } [get_ports { scl }]; #IO_L6P_T0_34 Sch=jd_p[2]
    set_property -dict { PACKAGE_PIN V7    IOSTANDARD LVCMOS33     } [get_ports { sda }]; #IO_L6N_T0_VREF_34 Sch=jd_n[2]
    set_property -dict { PACKAGE_PIN Y7    IOSTANDARD LVCMOS33     } [get_ports { dvs_n_ack }]; #IO_L11P_T1_SRCC_34 Sch=jd_p[3]
    set_property -dict { PACKAGE_PIN Y6    IOSTANDARD LVCMOS33     } [get_ports { dvs_n_req }]; #IO_L11N_T1_SRCC_34 Sch=jd_n[3]
    set_property -dict { PACKAGE_PIN V6    IOSTANDARD LVCMOS33     } [get_ports { imuint }]; #IO_L21P_T3_DQS_34 Sch=jd_p[4]
    set_property -dict { PACKAGE_PIN W6    IOSTANDARD LVCMOS33     } [get_ports { fsync }]; #IO_L21N_T3_DQS_34 Sch=jd_n[4]
    
    #Pmod Header JC
    set_property -dict { PACKAGE_PIN V15   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[0] }]; #IO_L4P_T0_34 Sch=je[1]
    set_property -dict { PACKAGE_PIN W15   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[1] }]; #IO_L18N_T2_34 Sch=je[2]
    set_property -dict { PACKAGE_PIN T11   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[2] }]; #IO_25_35 Sch=je[3]
    set_property -dict { PACKAGE_PIN T10   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[3] }]; #IO_L19P_T3_35 Sch=je[4]
    set_property -dict { PACKAGE_PIN W14   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[4] }]; #IO_L3N_T0_DQS_34 Sch=je[7]
    set_property -dict { PACKAGE_PIN Y14   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[5] }]; #IO_L9N_T1_DQS_34 Sch=je[8]
    set_property -dict { PACKAGE_PIN T12   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[6] }]; #IO_L20P_T3_34 Sch=je[9]
    set_property -dict { PACKAGE_PIN U12   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[7] }]; #IO_L7N_T1_34 Sch=je[10]

##### PMOD C and D (ZYBO, ZYBO Z7-10, ZYBO Z7-20)

    #Pmod Header JC
    set_property -dict { PACKAGE_PIN V15   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[8] }]; #IO_L5P_T0_34 Sch=jd_p[1]
    set_property -dict { PACKAGE_PIN W15   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[9] }]; #IO_L5N_T0_34 Sch=jd_n[1]
    set_property -dict { PACKAGE_PIN T11   IOSTANDARD LVCMOS33     } [get_ports { scl }]; #IO_L6P_T0_34 Sch=jd_p[2]
    set_property -dict { PACKAGE_PIN T10   IOSTANDARD LVCMOS33     } [get_ports { sda }]; #IO_L6N_T0_VREF_34 Sch=jd_n[2]
    set_property -dict { PACKAGE_PIN W14   IOSTANDARD LVCMOS33     } [get_ports { dvs_n_ack }]; #IO_L11P_T1_SRCC_34 Sch=jd_p[3]
    set_property -dict { PACKAGE_PIN Y14   IOSTANDARD LVCMOS33     } [get_ports { dvs_n_req }]; #IO_L11N_T1_SRCC_34 Sch=jd_n[3]
    set_property -dict { PACKAGE_PIN T12   IOSTANDARD LVCMOS33     } [get_ports { imuint }]; #IO_L21P_T3_DQS_34 Sch=jd_p[4]
    set_property -dict { PACKAGE_PIN U12   IOSTANDARD LVCMOS33     } [get_ports { fsync }]; #IO_L21N_T3_DQS_34 Sch=jd_n[4]
    
    #Pmod Header JD
    set_property -dict { PACKAGE_PIN T14   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[0] }]; #IO_L4P_T0_34 Sch=je[1]
    set_property -dict { PACKAGE_PIN T15   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[1] }]; #IO_L18N_T2_34 Sch=je[2]
    set_property -dict { PACKAGE_PIN P14   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[2] }]; #IO_25_35 Sch=je[3]
    set_property -dict { PACKAGE_PIN R14   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[3] }]; #IO_L19P_T3_35 Sch=je[4]
    set_property -dict { PACKAGE_PIN U14   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[4] }]; #IO_L3N_T0_DQS_34 Sch=je[7]
    set_property -dict { PACKAGE_PIN U15   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[5] }]; #IO_L9N_T1_DQS_34 Sch=je[8]
    set_property -dict { PACKAGE_PIN V17   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[6] }]; #IO_L20P_T3_34 Sch=je[9]
    set_property -dict { PACKAGE_PIN V18   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[7] }]; #IO_L7N_T1_34 Sch=je[10]

##### PMOD D and E (ZYBO, ZYBO Z7-10, ZYBO Z7-20)

    #Pmod Header JD
    set_property -dict { PACKAGE_PIN T14   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[8] }]; #IO_L5P_T0_34 Sch=jd_p[1]
    set_property -dict { PACKAGE_PIN T15   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[9] }]; #IO_L5N_T0_34 Sch=jd_n[1]
    set_property -dict { PACKAGE_PIN P14   IOSTANDARD LVCMOS33     } [get_ports { scl }]; #IO_L6P_T0_34 Sch=jd_p[2]
    set_property -dict { PACKAGE_PIN R14   IOSTANDARD LVCMOS33     } [get_ports { sda }]; #IO_L6N_T0_VREF_34 Sch=jd_n[2]
    set_property -dict { PACKAGE_PIN U14   IOSTANDARD LVCMOS33     } [get_ports { dvs_n_ack }]; #IO_L11P_T1_SRCC_34 Sch=jd_p[3]
    set_property -dict { PACKAGE_PIN U15   IOSTANDARD LVCMOS33     } [get_ports { dvs_n_req }]; #IO_L11N_T1_SRCC_34 Sch=jd_n[3]
    set_property -dict { PACKAGE_PIN V17   IOSTANDARD LVCMOS33     } [get_ports { imuint }]; #IO_L21P_T3_DQS_34 Sch=jd_p[4]
    set_property -dict { PACKAGE_PIN V18   IOSTANDARD LVCMOS33     } [get_ports { fsync }]; #IO_L21N_T3_DQS_34 Sch=jd_n[4]
    
    #Pmod Header JE
    set_property -dict { PACKAGE_PIN V12   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[0] }]; #IO_L4P_T0_34 Sch=je[1]
    set_property -dict { PACKAGE_PIN W16   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[1] }]; #IO_L18N_T2_34 Sch=je[2]
    set_property -dict { PACKAGE_PIN J15   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[2] }]; #IO_25_35 Sch=je[3]
    set_property -dict { PACKAGE_PIN H15   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[3] }]; #IO_L19P_T3_35 Sch=je[4]
    set_property -dict { PACKAGE_PIN V13   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[4] }]; #IO_L3N_T0_DQS_34 Sch=je[7]
    set_property -dict { PACKAGE_PIN U17   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[5] }]; #IO_L9N_T1_DQS_34 Sch=je[8]
    set_property -dict { PACKAGE_PIN T17   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[6] }]; #IO_L20P_T3_34 Sch=je[9]
    set_property -dict { PACKAGE_PIN Y17   IOSTANDARD LVCMOS33     } [get_ports { dvs_aer[7] }]; #IO_L7N_T1_34 Sch=je[10]
