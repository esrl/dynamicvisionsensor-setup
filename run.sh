#!/bin/sh

DEFAULT_DEVICE="/dev/bus/usb/001/"
DEFAULT_DOCKERFILE="Dockerfile"

DEVICE="/dev/bus/usb/001/"

print_usage()
{
	echo "Usage: program [-d | -u]"
	echo "Attach to-, start, run or build the required docker container."
	echo ""
	echo "-u\t--ubuntu\tUbuntu:18.04 (Bionic Beaver)"
	echo "-d\t--debian\tDebian:9 (Stretch)"
	echo "\t--device\tDevice file for the DVS camera."
	echo "\t\t\tUsually found in /dev/bus/usb/001/..."
}

OS="undefined"

for arg in "$@"
do
	echo $arg
	if [ "$arg" = "-d" ]
	then
		if [ "$OS" = "undefined" ]
		then
			OS="debian"
		else
			print_usage
			exit 1
		fi
	elif [ "$arg" = "-u" ]
	then
		if [ "$OS" = "undefined" ]
		then
			OS="ubuntu"
		else
			print_usage
			exit 1
		fi
	else
		print_usage
		exit 1
	fi
done

if [ "$OS" = "undefined" ]
then
	print_usage
	exit 1
elif [ "$OS" = "debian" ]
then
	DOCKERFILE="dockerfiles/Dockerfile-Debian_9"
	IMAGE_NAME="dvs_debian_image"
	CONTAINER_NAME="dvs_debian_container"
elif [ "$OS" = "ubuntu" ]
then
	DOCKERFILE="dockerfiles/Dockerfile-Ubuntu_18.04"
	IMAGE_NAME="dvs_ubuntu_image"
	CONTAINER_NAME="dvs_ubuntu_container"
fi

docker attach $CONTAINER_NAME
if [ $? -eq 0 ]; then
	exit 0
fi

xhost +local:`docker inspect --format='{{ .Config.Hostname }}' $CONTAINER_NAME`
docker start $CONTAINER_NAME
if [ $? -eq 0 ]; then
	$0 $1
	exit 0
fi

xhost +local:`docker inspect --format='{{ .Config.Hostname }}' $CONTAINER_NAME`
docker run -it -d \
	--net=host \
	--env="DISPLAY" \
	--env="QT_X11_NO_MITSHM=1" \
	--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	--device="/dev/video0:/dev/video0" \
	--volume="/etc/machine-id:/etc/machine-id" \
	--volume=$(dirname $(readlink -f $0))"/shared:/home/dev/shared/" \
	--device=$DEVICE \
	--name=$CONTAINER_NAME \
	$IMAGE_NAME\
	bash
if [ $? -eq 0 ]; then
	$0 $1
	exit 0
fi

docker build -f $DOCKERFILE -t "$IMAGE_NAME" .
if [ $? -eq 0 ]; then
	$0 $1
	exit 0
fi
echo "Can't find 'Dockerfile': $DOCKERFILE"
exit 1
