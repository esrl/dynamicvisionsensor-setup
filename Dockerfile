# Use ubuntu 18.04 LTS (Bionic-beaver) as base
FROM ubuntu:18.04

# Install java tools for jaer
RUN apt-get update
RUN apt-get install -y  software-properties-common
RUN add-apt-repository ppa:webupd8team/java -y
RUN apt-get update
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
RUN apt-get update
RUN apt-get install -y oracle-java8-installer
RUN apt-get install -y ant
RUN apt-get install -y ant-optional
# Install developer tools for libcaer
RUN apt-get install -y make
RUN apt-get install -y cmake
RUN apt-get install -y git
RUN apt-get install -y gcc g++
RUN apt-get install -y build-essential
RUN apt-get install -y pkg-config
RUN apt-get install -y libxxf86vm-dev
RUN apt-get install -y libxtst-dev
RUN apt-get install -y vim
RUN apt-get install -y libxrender-dev
RUN apt-get install -y libusb-1.0.0-dev
# Install firefox
RUN apt-get install -y firefox
# Clean up package list
RUN apt-get clean

# Create user
RUN useradd -m -s /bin/bash dev

# Clone jaer repository, compile the jaer code & create symbolic link
USER dev
WORKDIR /home/dev/
RUN git clone https://github.com/SensorsINI/jaer.git 
WORKDIR /home/dev/jaer/
RUN ant jar
USER root
RUN ln -s /jaer/jAERViewer_linux.sh jaer 

# installing libcaer & cleaning build directories
USER dev
WORKDIR /home/dev/
RUN git clone https://github.com/inivation/libcaer.git
WORKDIR /home/dev/libcaer/
RUN cmake -DCMAKE_INSTALL_PREFIX=/usr .
RUN make
USER root
RUN make install
USER dev
WORKDIR /home/dev/
RUN rm -r /home/dev/libcaer

USER root
RUN apt-get install oracle-java8-set-default
USER dev

# Goto home directory and start tail
WORKDIR /home/dev/
CMD tail
