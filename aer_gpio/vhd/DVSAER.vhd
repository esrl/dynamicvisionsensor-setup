library ieee;
use ieee.std_logic_1164.ALL;

entity DVSAER is
    port(
        clk : in std_logic := '0';
        
        nReset : in std_logic := '1';
        nEnable : in std_logic := '0';
        
        nAck_in : out std_logic := '1';
        nReq_in : in std_logic := '1';
        data_in : in std_logic_vector(9 downto 0) := (others => '0');
        
        nAck_out : in std_logic := '1';
        nReq_out : out std_logic := '1';        
        data_out : out std_logic_vector(16 downto 0) := (others => '0');
        
        x_out : out std_logic_vector(7 downto 0) := (others => '0');
        y_out : out std_logic_vector(7 downto 0) := (others => '0');
        p_out : out std_logic := '0';
        
        x_error : out std_logic := '0';
        y_error : out std_logic := '0'
    );
end DVSAER;

architecture behavioral of DVSAER is
    
    signal phase_in : integer range 0 to 1 := 0;
    signal phase_out : integer range 0 to 1 := 0;
    
    signal temp_data_out : std_logic_vector(16 downto 0) := (others => '0');
    
    signal x : std_logic_vector(7 downto 0) := (others => '0');
    signal y : std_logic_vector(7 downto 0) := (others => '0');
    signal p : std_logic := '0';
    
begin

    x_out <= x;
    y_out <= y;
    p_out <= p;

    data_out <= p & y & x;

    x_error <= '0' when (x < x"F0") else '1';
    y_error <= '0' when (y < x"B4") else '1';

    process (clk) begin
        if rising_edge(clk) then
            if (nReset = '0') then
                phase_in <= 0;
                phase_out <= 0;
                nAck_in <= '1';
                nReq_out <= '1';
                temp_data_out <= (others => '0');
            elsif (nEnable = '0') then
                if (nReq_in = '0') then
                    nAck_in <= '0';
                    if (phase_in = 0) then
                        phase_in <= 1;
                        if (data_in(9) = '0') then
                            y <= data_in(7 downto 0);
                            temp_data_out(15 downto 8) <= data_in(7 downto 0);
                        elsif (data_in(9) = '1') then
                            x <= data_in(8 downto 1);
                            temp_data_out( 7 downto 0) <= data_in(8 downto 1);
                            p <= data_in(0);
                            temp_data_out(16) <= data_in(0);
                            nReq_out <= '0';
                        end if;
                    end if;
                elsif (nReq_in = '1') then
                    nAck_in <= '1';
                    if (phase_in = 1) then
                        phase_in <= 0;
                    end if;
                end if;
                if (phase_out = 0 and nAck_out = '0') then
                    nReq_out <= '1';
                    phase_out <= 1;
                elsif (phase_out = 1 and nAck_out = '1') then
                    phase_out <= 0;
                end if;
            end if;
        end if;
    end process;
end behavioral;